# 1


# 'Rediscovering Simplicity' chapter in '99 Bottles of OOP', Metz

Title: 99 Bottles of OOP
Keywords: Rediscovering simplicity, Shameless Green, Consistency, Understandability, Changeability, Judging Code, Concretely Abstract 

**First Paragraph:**

The article, "Rediscovering Simplicity", is about object oriented programming and design. More specifically, the article creates a simple solution to the 99 bottles of beer song problem, and then applies a series of refactorings to improve the designs of the code. The article documents every step down every path of code, in javascript, providing a guided-tour of deicisons made along the way. It shows how good code looks when it is done, and reveals the thoughts that produced it. The key points of the article include, how writing simple code is a strength, optimizing code for understandability, focusing on changeability, and how design decisions inevitably involve tradeoffs. Walking through the code and referencing the solution to the problem is very easy to read. One part that stood out is when duplication, when you sing the song 99 bottles of beer - you will have multiple copies of the string "of beer" and "on the wall". String duplication is easy to see and understand but the logic is harder to comprehend then the actual data. (ie 1 bottle is singular but 2 bottles is plural).


**Second Paragraph:**

The article, "Rediscovering Simplicity", main strengths include the layout - 1.1 Simplyfing Code while making assertions (for instance having goals of writing concrete code and being abstract enough to allow for change), 1.2 Judging Code, 1.3 Summary, and going the extra length to include code snippets that pertain to the assertions. This flow makes the article really easy to understand and read even if you are not familiar with javascript or OOP. Additionally, the terminology boxes make it very simple to understand why certain methods are structured the way they are. There really isn't any weaknesses or drawbacks of this article. One of my favorite takeaways from this article is that "writing code is like writing a book; your efforts are for other readers".


# 'Modern Code Review' chapter in 'Making Software'

Title: Chapter 18. Modern Code Review
Keywords: Common sense, examining, code review, data

**First Paragraph:**

The article, "Modern Code Review", aims to ask the  question of when is code review useful, and when it is, what are the best practices? The article starts off explaining commmon sense and that two heads are better than one. Thereafter, we take a look at the data of focus fatigue, and discover that every 10 minutes another defect is found (on average) but after 60 minutes there is a sudden drop off - maybe the code reveiwer gets a little lazy or fatigued? The article the explains group dynamics and brings up 'formal inspection' known as Fagan's theory.. Four people meeting for two hours is eight person-hours which is a person-day of time. The article concludes that it is not about asking whether code review is good or not but when it is useful, what are the best practices. 


**Second Paragraph:**

Overall I thought the article was short and to the point. It was easy to understand, it brought up great points, and it was interesting to find out the statistics of finding errors for code reviews and when focus fatigue kicks in. I think the article could have went a little more in depth about Fagan's theory - but overall I enjoyed it . 

**Notes** 

Common Sense - two heads are better than one 
Code review also occupies expensive developer time 
Do the benefits outweigh the costs? 

One commonality of code review is that one developer critically examines the code 
Developers dive into code by themselves, without distraction, and without discussion - what does the data say? 
Every 10 minutes another defect is found (on average) - the more time spent in reviews the more defects are found 
Around the 60 minute mark there is a sudden drop-off

Code review tends to be done together. 
Are meetings required? the practice is a potential utter waste of time 
the focal point of the process is a two-hour meeting where four participants discuss the code. - Michael Fagan 

Is good review good or bad? - not the question 
rather the question is - When is code review useful, and when it is, what are the best practices ? 

The studies given here address best-practices—preventing you from systematically and senselessly wasting time.


# Read code review checklist on pages 15-18 in Greiler Code Review EBook

Title: Read code review checklist on pages 15-18 in Greiler Code Review EBook
Keywords: Motivations, Pitfalls, Checklist, Survey, Process

**First Paragraph**

The powerpoint, Greiler Code Review, is modeled after a full-day workshop designed to boost code review practice by Dr. Michaela Greiler.
The powerpoint starts out introducing a code review study conducted by Microsoft to better understand developers' needs and best practices during code reviewing.
Out of 75% engineers and 25% managers, 39% responded that they review changes of others at least once a day, 21% of the respondents review multiple changes per day,
36% review code multiple times per week, and 13% either review once per week or do not participate in code reviews in the last week. The takeaway was developers do 
code reviews to improve the code, to find defects, to transfer knowledge about the code, and to find alternative solutions. The powerpoint has an awesome slide on 
Code Reveiw best practices (too long to list), and affirms that code reviews are an integral part of the development process. Additionally, the powerpoint displays a great 
slide on pitfalls - waiting for code feedback is a pain, and not getting valuable feedback decreases the developers' benefit from and motivation for code reviews. 

**Second Paragraph**

Overall, the strengths of the powerpoint were the motivations behind code reviews, the best practices, and the pitfalls. I do not think it was neccessary to include some of 
the questions on the checklist that seem like common sense - but overall it was a nicely concise powerpoint.

# 10 Tips for Respectful and Constructive Code Review Feedback

Title: 10 Tips for Respectful and Constructive Code Review Feedback
Keywords: Ask Questions, Make Feedback about Code, Perspective, No Sarcasm, No Condenscending Words, Use Emojis, Explains Reasons, Add Value, Assume Miscommunication over Malice

**First Paragraph**

The video, Tips for Respectful and Constructive Code Review Feedback, was about tips for giving respectful yet constructive code review feedback. 
Dr. McKayla explains ten tips (listed in keywords) and explains her reasonsing for each tip. Overall the video was really helpful and the ten 
tips made a ton of sense.

**Second Paragraph**

The strengths of the video is that it's to the point, and the reasoning behind each tip is extremely helpful. I never thought about using emoji's outside of text messages 
but it's important to use them when you give constructive feedback and it _could_ sound condenscending. There were no weaknesses for this video, she even went on to give examples past her reasoning. Overall a great video that I will reference when doing reviews. 

#  SOLID Object-Oriented Design, Sandi Metz (https://www.youtube.com/watch?v=v-2yFMzxqwU)

Title: GORUCO 2009 - SOLID Object-Oriented Design by Sandi Metz
Keywords: Rigid, Fragile, Immobile, Viscous, Design Stamina Hypothesis  

**First Paragraph**

The video,  SOLID Object-Oriented Design, is presentation by Sandy Metz from Duke University. She starts out by talking about apps that she has written/maintenanced for Duke, such as the grant management system application, that has been around since the 1990s. Furthermore, Sandy goes into how apps change and it is dependent on design. 
For instance, with rigid applications if you make one small change - it will cause a cascade of related changes. Rotting software applications may be rigid, fragile, immobile, or viscosity. The presentation is about changes. Applications do not start out with these qualities listed above - design can save us. Sandy suggests we should skip design if you plan for your application to fail, but if you succeed it will cost you money down the road. The main gist of the presentation is talking about the paper SOLID Design principles and patterns by Robert Martin. SOLID design principles include <strong>S</strong>ingle Responsibility, <strong>O</strong>pen/Closed, <strong>L</strong>iskov Substitution, <strong>I</strong>nterface Segregation, and <strong>D</strong>ependency Inversion. All of these topics are about managing the dependencies in our application. Overall, these principles are strategies we can apply to lead us to a place where your application has minimal entanglements with eachother so we can change things. 

**Second Paragraph**

This video has a ton of strengths, the main strength is walking throught the code snippet of the sample application. We have code that runs, passes the tests, and does what it's supposed to - but the code does not tolerate change. Sandy walks through how we can apply SOLID rules, and what we should do step by step to improve the code. Another strength is that if you are having trouble writing tests - chances our your design is terrible. Lastly, there are no weaknesses. The video was very informative and directly applicable. 

**notes**

- Single Responsibility - There should never be more than one reason for a class to change 
- Liskov Subsitition - subclassing 
- Interface segregation - many client specific interfaces are better than one general purpose interface 
- Dependence Inversion - depend upon abstraction 

- how can this schtuff help?? 
- how do we get there
- Well, to avoid dependencies our code should be : 
- loosely coupled - inject dependencies 
- highly cohesive - a class should all be about the same thing 
- easily composable - more dependencies 
- context independent - more dependencies 

- interface segregation is something we care about for staticly typed languages - c++ or java - dealing with other classes 
- Liskov is about subclassing, if you create a class foo and create a sublass fooish - any place you use a foo you ought to be able to subsitute fooish 

- Code walk through of an FTP server with a csv on it - we want to get the file / parse it / and store it in a database on our side
- Patent job should do these two things: download the file and store it into the database 
- Job should have an api that includes download file
- Job should have a method that says run 
- Class says run has a method download file
- Are we done ? Code works , does what it supposed to do.. 
- The code doesn't tolerate change - you have to download the file everytime you want have to run a test 

- Is it dry (it repeat istelf) ? Are there duplicated code in any of the classes? 
- Does it have one responsibility ? 
- Does each class contain things that change at the same rate ? 
- Does it depend on things that change less often than it does ? 


**Code Review 1 [2-4 hours, 2 points]**

99 Bottles of Beer: 

Which of the four styles Metz presents most closely matches the code you wrote? 

I would say my 99 bottles of beer code is between speculatively general and shameless green - if you disregard the method  I wrote.
The reason I say this is because it is not unnecessary complex, but it's not super simple. 
I could have made the branching statement make more sense like the ones Dr. Slankas showed us in class but the only reason my SLOC was 
high was because I had trouble using the StringBuilder when writing the last method. 

How difficult was it to write?

It was easy to write aside from the last test. 

How hard is it to understand?

It is not hard to understand. 

How expensive will it be to change?

It will be cheap to change. Even though I have a few branching statements and a new variable decrement for verse - 
it is easy to change and track the changes. 

What are the SLOC and ABC metrics for the code?

The SLOC metrics for the code are 261 lines. I am ashamed of this and the only reason it's this long is because of my last method. 
I did not know how to use string builder - so I copied the string expected and used it to pass the last test. ABC: 11
Assignments: 3
Branches: 4
Conditions: 4

How does the 'DRY' principle apply to your code?

There is a lot of duplication for the last method song, I should have used a for loop "for (int i = 99; i > 1; i--)"
and greatfully reduced the amount of lines that "bottles of beer on the wall, bottles of beer" is written. 
Other than that, there isn't a whole lot of duplicated code. 

KWIC: 

Which of the four styles Metz presents most closely matches the code you wrote? 

The style of code that matches the code I wrote for KWIC is shameless green. 
I believe I solved the problem in a simplistic way and the code is gratifyingly easy to understand. 
I even wrote comments for each block that was doing different stuff. 

How difficult was it to write?

It was medium to difficult to write. 

How hard is it to understand?

I would say the first two parts of the code are easier to understand than the last. 

How expensive will it be to change?

It will be pretty expensive to change. The logic of 'int lastId = -1' if changed will not uppercase the second MAN in the output. 
It will be easy to change how the code reads in lines, but for walking the words this could mess up a lot of things. 

What are the SLOC and ABC metrics for the code?



The SLOC metrics for the code are 78 lines of code. The ABC score is 20. 
Assignments: 10
Branches: 6
Conditions: 4

How does the 'DRY' principle apply to your code?

There is a decent amount of duplication in my code. For instance the first two parts seperated by comments - shows I could have made a parse 
method that while the input has the next line, store the input in a string called line. The only problem is I break in the first part for :: vs 
forcing to lowercase and splitting titles in the next part. So, I could have made a parse method but I was having trouble implementing this. 

**Troubles with the Efficiency Frontier**

So, looking at my 64 code, I was able to create two classes - one for investment and the other for portfolio. I created a constructor for each and even implemented random weights. I was even able to successfully take in the two command line arguments with the sample data given. The trouble I had was with the efficiency frontier and the Eigen package. Breaking up the assignment into steps was easy for me, but some of the calculations didn't rest well. I guess I do not have the domain knowledge, and the added complexity is intimidating. Overall, I will complete this assignment in due time. I will use the sample data and build a model in excel - that way I can solve for the output and be able to translate it into code in an easier fashion. Additionally, moving from C to C++ without going over fields, constructors, getters, setters, methods, etc was not the smoothest transition. Luckily I was able to learn quick from previous knowledge of Java and youtube. 

**Reflection**

Everything went well, and according to plan. What didn't go well was being able to contribute my 64 code because I have not finished the assignment, yet. I wish we could have worked together as a group on a quick project and did a code review together instead of basing this assignment on code from last semester that not everyone has completed. 

# 2 

<https://gitlab.oit.duke.edu/mrc90/ft512-assignment3>

# 3 

# Times for tasks and additional notes

* 'Rediscovering Simplicity' chapter in '99 Bottles of OOP', Metz + write up - 1hr 30mins
* Chapter 18. Modern Code Review + write up - 45 mins 
* Read code review checklist on pages 15-18 + write up - 30 mins 
* SOLID Object-Oriented Design, Sandi Metz + write up - 1hr 30 mins
* Code Review Tips, Greiler + write up - 25 mins 
* Code Review 1 - 2 hours 
* Code Review 2 - 1 hour 
